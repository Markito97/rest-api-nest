require("dotenv").config()
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

const start = async () => {
  try {
    const zopa = "abobys";
    const PORT = process.env.PORT || 7000;
    const app = await NestFactory.create(AppModule)

    await app.listen(PORT, () => console.log(`Server stated on ${PORT}`))
  } catch (err) {
    console.log(err)
  }
}

start(); 