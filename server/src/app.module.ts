import { Module } from '@nestjs/common';
import { SampleModule } from './sample/sample.module';
import { MongooseModule } from "@nestjs/mongoose";

@Module({
  imports: [
    MongooseModule.forRoot("mongodb://localhost/rest-api"),
    SampleModule
  ]
})
export class AppModule {}
