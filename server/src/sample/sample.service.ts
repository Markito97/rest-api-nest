import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { SampleDocument, Sample } from "./models/sample.schema";
import { Model } from "mongoose";
import { CreateSampleDto } from "./dto/create-sample.dto";
import mongoose from "mongoose";

@Injectable()
export class SampleService {
    
    constructor(@InjectModel(Sample.name) private sampleModel: Model<SampleDocument>) { }

    async create(dto: CreateSampleDto): Promise<Sample> {
        const sample = await this.sampleModel.create({ ...dto })
        return sample;
    }

    async getAll(): Promise<Sample[]> {
        const samples = await this.sampleModel.find()
        return samples;
    }

    async getOne(id: mongoose.Types.ObjectId): Promise<Sample> {
        const sample = await this.sampleModel.findById(id);
        return sample;
    }

    async delete(id: mongoose.Types.ObjectId): Promise<mongoose.Types.ObjectId> {
        const sample = await this.sampleModel.findByIdAndDelete(id)
        return sample._id;
    }
}