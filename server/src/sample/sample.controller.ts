import { Controller, Get, Post, Body, Param, Delete } from "@nestjs/common";
import { CreateSampleDto } from "./dto/create-sample.dto";
import { SampleService } from "./sample.service";
import mongoose from "mongoose";

@Controller('/sample')
export class SampleContoller {
    constructor(private sampleService: SampleService) { }

    @Post()
    create(@Body() dto: CreateSampleDto) {
        return this.sampleService.create(dto)
    }

    @Get()
    getAll() {
        return this.sampleService.getAll()
    }

    @Get(":id")
    getOne(@Param("id") id: mongoose.Types.ObjectId) {
        return this.sampleService.getOne(id)
    }

    @Delete(":id")
    delete(@Param("id") id: mongoose.Types.ObjectId) {
        return this.sampleService.delete(id)
    }
}