import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type SampleDocument = Sample & Document;

@Schema()
export class Sample {
    @Prop()
    name: string;

    @Prop()
    age: number;

}

export const SampleSchema = SchemaFactory.createForClass(Sample);