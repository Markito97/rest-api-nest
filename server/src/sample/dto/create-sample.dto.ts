export class CreateSampleDto {
  readonly name: string;
  readonly age: number;
}
