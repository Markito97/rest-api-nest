import { Module } from "@nestjs/common";
import { SampleContoller } from "./sample.controller";
import { SampleService } from "./sample.service";
import { MongooseModule } from "@nestjs/mongoose";
import { Sample, SampleSchema } from "./models/sample.schema";

@Module({
    imports: [
        MongooseModule.forFeature([{ name: Sample.name, schema: SampleSchema }])
    ],
    controllers: [SampleContoller],
    providers: [SampleService]
})
export class SampleModule { }
